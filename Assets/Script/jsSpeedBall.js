#pragma strict

private var speed : float;
private var rotation : float;

function Update () {
	var amtMove = speed * Time.deltaTime;
	transform.Translate(transform.forward * amtMove , Space.World);
	
	if(transform.position.z < -12 || transform.position.z > 2)
	{
		Destroy(gameObject);
	}
}

function OnTriggerEnter(coll : Collider)
{
	if(coll.tag == "FENCE" || coll.tag == "TOPFENCE")
	{
		CheckBounds(coll);
		coll.gameObject.audio.Play();
	}
	else if( coll.gameObject.tag.Substring(0,5) == "BLOCK")
	{
		CheckBounds(coll);
		coll.gameObject.SendMessage("SetCollision" , false , SendMessageOptions.DontRequireReceiver);
	}
}

function CheckBounds(coll : Collider)
{
	if(coll.tag == "TOPFENCE")
	{
		transform.rotation.eulerAngles.y = 180 - transform.rotation.eulerAngles.y;
		return;
	}
	
	if(coll.tag == "FENCE")
	{
		transform.rotation.eulerAngles.y = 360 - transform.rotation.eulerAngles.y;
	}
	
	var bounds : Bounds = coll.bounds;
	var pos : Vector3 = transform.position;
	var w = transform.localScale.x / 2;
	
	if(Mathf.Abs(bounds.center.x - pos.x ) < bounds.extents.x + w)
	{
		transform.rotation.eulerAngles.y = 180 - transform.rotation.eulerAngles.y;
	}
	else
	{
		transform.rotation.eulerAngles.y = 360 - transform.rotation.eulerAngles.y;
	}
}

function SetSpeed()
{
	speed = Random.Range(5.0 , 8.0);
	rotation =Random.Range(-45.0 , 45.0);
	transform.rotation.eulerAngles.y = rotation;
}